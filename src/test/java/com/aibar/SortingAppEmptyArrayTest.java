package com.aibar;


import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


import static org.junit.Assert.*;

public class SortingAppEmptyArrayTest {

    protected SortingApp sortingApp = new SortingApp();

    @Test
    public void testEmptyCase() {
        String userInput = "";
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        String expected = "The array is empty";
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        SortingApp.main(null);

        String[] lines = baos.toString().split(System.lineSeparator());
        String actual = lines[lines.length-1];
        while(actual.charAt(actual.length() - 1) == ' ')
            actual = actual.substring(0, actual.length() - 1);

        assertEquals(expected, actual);
    }
}
