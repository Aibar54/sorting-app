package com.aibar;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingAppSingleElementArrayTest {
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {"3", "3"},
                {"2", "2"},
                {"1", "1"},
                {"35", "35"},
                {"-1", "-1"},
                {"0", "0"},
                {"-13245", "-13245"},
                {"3145691", "3145691"}
        });
    }

    protected SortingApp sortingApp = new SortingApp();

    private final String userInput;
    private final String expected;

    public SortingAppSingleElementArrayTest(String userInput, String expected) {
        this.userInput = userInput;
        this.expected = expected;
    }


    @Test
    public void testSingleElementCase() {
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        SortingApp.main(null);

        String[] lines = baos.toString().split(System.lineSeparator());
        String actual = lines[lines.length-1];
        while(actual.charAt(actual.length() - 1) == ' ')
            actual = actual.substring(0, actual.length() - 1);

        assertEquals(expected, actual);
    }
}
