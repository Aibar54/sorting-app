package com.aibar;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingAppTenElementsArrayTest {
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {"10 9 8 7 6 5 4 3 2 1", "1 2 3 4 5 6 7 8 9 10"},
                {"123 34 0 -12 0 132 -2 9 1333337 16", "-12 -2 0 0 9 16 34 123 132 1333337"},
                {"0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 0 0 0"},
                {"-1 -2 -3 -4 -5 -6 -7 -8 -9 -10", "-10 -9 -8 -7 -6 -5 -4 -3 -2 -1"}
        });
    }

    protected SortingApp sortingApp = new SortingApp();

    private final String userInput;
    private final String expected;

    public SortingAppTenElementsArrayTest(String userInput, String expected) {
        this.userInput = userInput;
        this.expected = expected;
    }


    @Test
    public void testTenElementsCase() {
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        SortingApp.main(null);

        String[] lines = baos.toString().split(System.lineSeparator());
        String actual = lines[lines.length-1];
        while(actual.charAt(actual.length() - 1) == ' ')
            actual = actual.substring(0, actual.length() - 1);

        assertEquals(expected, actual);
    }
}
