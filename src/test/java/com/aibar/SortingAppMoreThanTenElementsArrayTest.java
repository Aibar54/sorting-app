package com.aibar;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingAppMoreThanTenElementsArrayTest {
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList("3 2 1 4 5 6 7 9 10 8 11",
                "2 1 1 1 1 1 1 1 1 1 1 2 3 4 5",
                "123 23 32 41 45 0 0 -1 -1 1337 2 12 312 1 1 1 1 01 1 1 1 1 1 1 1 1",
                "34 20 0 0 -1 -228 314 2 1 3 23");
    }

    protected SortingApp sortingApp = new SortingApp();

    private final String userInput;

    public SortingAppMoreThanTenElementsArrayTest(String userInput) {
        this.userInput = userInput;
    }


    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenElementsCase() {
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        SortingApp.main(null);
    }
}
