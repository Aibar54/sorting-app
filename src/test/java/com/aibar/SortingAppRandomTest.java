package com.aibar;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingAppRandomTest {
    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {"3 2 1", "1 2 3"},
                {"2 1", "1 2"},
                {"1 2 3 4", "1 2 3 4"},
                {"3 1 2 4", "1 2 3 4"},
                {"1 1 1", "1 1 1"},
                {"2 1 2 1", "1 1 2 2"},
                {"43 34 -11 0 1", "-11 0 1 34 43"},
                {"-1 -2 0 1", "-2 -1 0 1"}
        });
    }

    protected SortingApp sortingApp = new SortingApp();

    private final String userInput;
    private final String expected;

    public SortingAppRandomTest(String userInput, String expected) {
        this.userInput = userInput;
        this.expected = expected;
    }


    @Test
    public void testRandomCase() {
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        SortingApp.main(null);

        String[] lines = baos.toString().split(System.lineSeparator());
        String actual = lines[lines.length-1];
        while(actual.charAt(actual.length() - 1) == ' ')
            actual = actual.substring(0, actual.length() - 1);

        assertEquals(expected, actual);
    }
}
