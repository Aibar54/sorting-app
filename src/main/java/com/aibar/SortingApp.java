package com.aibar;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SortingApp {
    public static void input(ArrayList<Integer> array){
        Scanner scanner = new Scanner(System.in);
        int i = 0;
        while(scanner.hasNext()){
            if(i == 10)
                throw new IllegalArgumentException("Array must contain less than 10 elements");
            array.add(scanner.nextInt());
            i++;
        }
    }

    public static void output(ArrayList<Integer> array){
        if(array.size() == 0){
            System.out.println("The array is empty");
            return;
        }
        System.out.println("Sorted array: ");
        for(Integer x : array)
            System.out.print(x + " ");
    }

    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<>();
        System.out.println("Enter the array:");
        input(array);
        Collections.sort(array);
        output(array);
    }
}
